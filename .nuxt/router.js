import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _097560c8 = () => import('../pages/educations/index.vue' /* webpackChunkName: "pages/educations/index" */).then(m => m.default || m)
const _2074a041 = () => import('../pages/users/index.vue' /* webpackChunkName: "pages/users/index" */).then(m => m.default || m)
const _7ae0293f = () => import('../pages/states/index.vue' /* webpackChunkName: "pages/states/index" */).then(m => m.default || m)
const _27c27722 = () => import('../pages/profile/index.vue' /* webpackChunkName: "pages/profile/index" */).then(m => m.default || m)
const _719cf7b6 = () => import('../pages/recruiters/index.vue' /* webpackChunkName: "pages/recruiters/index" */).then(m => m.default || m)
const _5f18edc2 = () => import('../pages/login.vue' /* webpackChunkName: "pages/login" */).then(m => m.default || m)
const _7ad2165b = () => import('../pages/addresses/index.vue' /* webpackChunkName: "pages/addresses/index" */).then(m => m.default || m)
const _ea2f8600 = () => import('../pages/talents/index.vue' /* webpackChunkName: "pages/talents/index" */).then(m => m.default || m)
const _092969c6 = () => import('../pages/cities/index.vue' /* webpackChunkName: "pages/cities/index" */).then(m => m.default || m)
const _3e99a9aa = () => import('../pages/resume/index.vue' /* webpackChunkName: "pages/resume/index" */).then(m => m.default || m)
const _47f68afd = () => import('../pages/filter_talents/index.vue' /* webpackChunkName: "pages/filter_talents/index" */).then(m => m.default || m)
const _8e5816fe = () => import('../pages/user_groups/index.vue' /* webpackChunkName: "pages/user_groups/index" */).then(m => m.default || m)
const _70b528a5 = () => import('../pages/filter_talents/_id.vue' /* webpackChunkName: "pages/filter_talents/_id" */).then(m => m.default || m)
const _06cbe608 = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/educations",
			component: _097560c8,
			name: "educations"
		},
		{
			path: "/users",
			component: _2074a041,
			name: "users"
		},
		{
			path: "/states",
			component: _7ae0293f,
			name: "states"
		},
		{
			path: "/profile",
			component: _27c27722,
			name: "profile"
		},
		{
			path: "/recruiters",
			component: _719cf7b6,
			name: "recruiters"
		},
		{
			path: "/login",
			component: _5f18edc2,
			name: "login"
		},
		{
			path: "/addresses",
			component: _7ad2165b,
			name: "addresses"
		},
		{
			path: "/talents",
			component: _ea2f8600,
			name: "talents"
		},
		{
			path: "/cities",
			component: _092969c6,
			name: "cities"
		},
		{
			path: "/resume",
			component: _3e99a9aa,
			name: "resume"
		},
		{
			path: "/filter:talents",
			component: _47f68afd,
			name: "filtertalents"
		},
		{
			path: "/user:groups",
			component: _8e5816fe,
			name: "usergroups"
		},
		{
			path: "/filter:talents/:id",
			component: _70b528a5,
			name: "filtertalents-id"
		},
		{
			path: "/",
			component: _06cbe608,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
