import Vue from 'vue'
import NuxtLoading from './components/nuxt-loading.vue'

import '../assets/css/style.css'

import '../plugins/metronic/global/plugins/font-awesome/css/font-awesome.min.css'

import '../plugins/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css'

import '../plugins/metronic/global/plugins/bootstrap/css/bootstrap.min.css'

import '../plugins/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'

import '../plugins/metronic/global/plugins/bootstrap-toastr/toastr.min.css'

import '../plugins/metronic/global/css/components.min.css'

import '../plugins/metronic/global/css/plugins.min.css'

import '../plugins/metronic/layouts/layout/css/layout.min.css'

import '../plugins/metronic/layouts/layout/css/themes/darkblue.min.css'

import '../plugins/metronic/layouts/layout/css/custom.min.css'


let layouts = {

  "_default": () => import('../layouts/default.vue'  /* webpackChunkName: "layouts/default" */).then(m => m.default || m),

  "_empty": () => import('../layouts/empty.vue'  /* webpackChunkName: "layouts/empty" */).then(m => m.default || m)

}

let resolvedLayouts = {}

export default {
  head: {"title":"TALENT HUT","meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"description","name":"description","content":"Admin panel"},{"name":"msapplication-TileColor","content":"#ffffff"},{"name":"msapplication-TileImage","content":"\u002Ffav1\u002Fms-icon-144x144.png"},{"name":"theme-color","content":"#ffffff"}],"link":[{"rel":"apple-touch-icon","sizes":"57x57","href":"\u002Ffav1\u002Fapple-icon-57x57.png"},{"rel":"apple-touch-icon","sizes":"60x60","href":"\u002Ffav1\u002Fapple-icon-60x60.png"},{"rel":"apple-touch-icon","sizes":"72x72","href":"\u002Ffav1\u002Fapple-icon-72x72.png"},{"rel":"apple-touch-icon","sizes":"76x76","href":"\u002Ffav1\u002Fapple-icon-76x76.png"},{"rel":"apple-touch-icon","sizes":"114x114","href":"\u002Ffav1\u002Fapple-icon-114x114.png"},{"rel":"apple-touch-icon","sizes":"120x120","href":"\u002Ffav1\u002Fapple-icon-120x120.png"},{"rel":"apple-touch-icon","sizes":"144x144","href":"\u002Ffav1\u002Fapple-icon-144x144.png"},{"rel":"apple-touch-icon","sizes":"152x152","href":"\u002Ffav1\u002Fapple-icon-152x152.png"},{"rel":"apple-touch-icon","sizes":"180x180","href":"\u002Ffav1\u002Fapple-icon-180x180.png"},{"rel":"icon","type":"image\u002Fx-icon","sizes":"192x192","href":"\u002Ffav1\u002Fandroid-icon-192x192.png"},{"rel":"icon","type":"image\u002Fx-icon","sizes":"32x32","href":"\u002Ffav1\u002Ffavicon-32x32.png"},{"rel":"icon","type":"image\u002Fx-icon","sizes":"96x96","href":"\u002Ffav1\u002Ffavicon-96x96.png"},{"rel":"icon","type":"image\u002Fx-icon","sizes":"16x16","href":"\u002Ffav1\u002Ffavicon-16x16.png"},{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Ffav1\u002Ffavicon.ico"},{"rel":"manifest","href":"\u002Ffav1\u002Fmanifest.json"}],"script":[],"style":[]},
  render(h, props) {
    const loadingEl = h('nuxt-loading', { ref: 'loading' })
    const layoutEl = h(this.layout || 'nuxt')
    const templateEl = h('div', {
      domProps: {
        id: '__layout'
      },
      key: this.layoutName
    }, [ layoutEl ])

    const transitionEl = h('transition', {
      props: {
        name: 'layout',
        mode: 'out-in'
      }
    }, [ templateEl ])

    return h('div',{
      domProps: {
        id: '__nuxt'
      }
    }, [
      loadingEl,
      transitionEl
    ])
  },
  data: () => ({
    layout: null,
    layoutName: ''
  }),
  beforeCreate () {
    Vue.util.defineReactive(this, 'nuxt', this.$options.nuxt)
  },
  created () {
    // Add this.$nuxt in child instances
    Vue.prototype.$nuxt = this
    // add to window so we can listen when ready
    if (typeof window !== 'undefined') {
      window.$nuxt = this
    }
    // Add $nuxt.error()
    this.error = this.nuxt.error
  },
  
  mounted () {
    this.$loading = this.$refs.loading
  },
  watch: {
    'nuxt.err': 'errorChanged'
  },
  
  methods: {
    
    errorChanged () {
      if (this.nuxt.err && this.$loading) {
        if (this.$loading.fail) this.$loading.fail()
        if (this.$loading.finish) this.$loading.finish()
      }
    },
    
    setLayout (layout) {
      if (!layout || !resolvedLayouts['_' + layout]) layout = 'default'
      this.layoutName = layout
      let _layout = '_' + layout
      this.layout = resolvedLayouts[_layout]
      return this.layout
    },
    loadLayout (layout) {
      if (!layout || !(layouts['_' + layout] || resolvedLayouts['_' + layout])) layout = 'default'
      let _layout = '_' + layout
      if (resolvedLayouts[_layout]) {
        return Promise.resolve(resolvedLayouts[_layout])
      }
      return layouts[_layout]()
      .then((Component) => {
        resolvedLayouts[_layout] = Component
        delete layouts[_layout]
        return resolvedLayouts[_layout]
      })
      .catch((e) => {
        if (this.$nuxt) {
          return this.$nuxt.error({ statusCode: 500, message: e.message })
        }
      })
    }
  },
  components: {
    NuxtLoading
  }
}

