module.exports = {
    /*
    ** Headers of the page
    */
    mode:'spa',

    head: {
        title: 'TALENT HUT',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Admin panel' },
            { name: 'msapplication-TileColor', content: '#ffffff' },
            { name: 'msapplication-TileImage', content: '/fav1/ms-icon-144x144.png' },
            { name: 'theme-color', content: '#ffffff' }
        ],
        link: [
            { rel: 'apple-touch-icon', sizes: '57x57', href: '/fav1/apple-icon-57x57.png' },
            { rel: 'apple-touch-icon', sizes: '60x60', href: '/fav1/apple-icon-60x60.png' },
            { rel: 'apple-touch-icon', sizes: '72x72', href: '/fav1/apple-icon-72x72.png' },
            { rel: 'apple-touch-icon', sizes: '76x76', href: '/fav1/apple-icon-76x76.png' },
            { rel: 'apple-touch-icon', sizes: '114x114', href: '/fav1/apple-icon-114x114.png' },
            { rel: 'apple-touch-icon', sizes: '120x120', href: '/fav1/apple-icon-120x120.png' },
            { rel: 'apple-touch-icon', sizes: '144x144', href: '/fav1/apple-icon-144x144.png' },
            { rel: 'apple-touch-icon', sizes: '152x152', href: '/fav1/apple-icon-152x152.png' },
            { rel: 'apple-touch-icon', sizes: '180x180', href: '/fav1/apple-icon-180x180.png' },
            { rel: 'icon', type: 'image/x-icon', sizes: '192x192', href: '/fav1/android-icon-192x192.png' },
            { rel: 'icon', type: 'image/x-icon', sizes: '32x32', href: '/fav1/favicon-32x32.png' },
            { rel: 'icon', type: 'image/x-icon', sizes: '96x96', href: '/fav1/favicon-96x96.png' },
            { rel: 'icon', type: 'image/x-icon', sizes: '16x16', href: '/fav1/favicon-16x16.png' },
            { rel: 'icon', type: 'image/x-icon', href: '/fav1/favicon.ico' },
            { rel: 'manifest', href: '/fav1/manifest.json' }
        ],
        script: [
        ]
    },

    css: [
        '~/assets/css/style.css',
        '~/plugins/metronic/global/plugins/font-awesome/css/font-awesome.min.css',
        '~/plugins/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css',
        '~/plugins/metronic/global/plugins/bootstrap/css/bootstrap.min.css',
        '~/plugins/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        '~/plugins/metronic/global/plugins/bootstrap-toastr/toastr.min.css',
        '~/plugins/metronic/global/css/components.min.css',
        '~/plugins/metronic/global/css/plugins.min.css',
        '~/plugins/metronic/layouts/layout/css/layout.min.css',
        '~/plugins/metronic/layouts/layout/css/themes/darkblue.min.css',
        '~/plugins/metronic/layouts/layout/css/custom.min.css'
    ],


    /*
    ** Customize the progress bar color
    */
    loading: { color: '#3B8070' },
    /*
    ** Build configuration
    */
    build: {
        vendor: [],
        extend (config, { isDev, isClient }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },


    plugins: [
        { src: '~/plugins/metronic/global/plugins/jquery.min.js', ssr: false },
        { src: '~/plugins/metronic/global/plugins/bootstrap/js/bootstrap.min.js', ssr: false },
        { src: '~/plugins/metronic/global/plugins/js.cookie.min.js', ssr: false },
        { src: '~/plugins/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js', ssr: false },
        { src: '~/plugins/metronic/global/plugins/jquery.blockui.min.js', ssr: false },
        { src: '~/plugins/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js', ssr: false },
        { src: '~/plugins/metronic/global/plugins/bootstrap-toastr/toastr.min.js', ssr: false },
        { src: '~/plugins/metronic/global/scripts/app.js', ssr: false },
        { src: '~/plugins/helper', ssr: false },
        { src: '~/plugins/chelper', ssr: false },
        { src: '~/plugins/vue-sweetalert', ssr: false },
        { src: '~/plugins/vee-validate', ssr: true },
        { src: '~/plugins/masked-input', ssr: true },
        { src: '~/plugins/vue-tables-2', ssr: false },
        { src: '~/plugins/vue-toastr', ssr: false },
        { src: '~/plugins/vue-select', ssr: false },
        { src: '~/plugins/vue-color-picker', ssr: false },
        { src: '~/plugins/vue-chart-js', ssr: false },
        { src: '~/plugins/vue-checkbox-radio', ssr: false },
        //{ src: '~/plugins/vue-highchart', ssr: false },
        { src: '~/plugins/vue-slimscroll', ssr: false },
        { src: '~/plugins/vue-print', ssr: false },
        { src: '~/plugins/vue-json-pretty', ssr: false },
        { src: '~/plugins/common-components'},
    ]
}
