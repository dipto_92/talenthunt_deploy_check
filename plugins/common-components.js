import Vue from 'vue'
import Modal from '~/components/common/Modal.vue'
import VueLadda from 'vue-ladda'

const components = { Modal, VueLadda}

Object.keys(components).forEach(key => {
  Vue.component(key, components[key])
})
